**deploy.js** is a DevOps node module to manage AS Group updates
# Install

```
git clone git@gitlab.com:ateather/aws-deploy-devops.git
npm install
node deploy.js --install
```

Now you need to setup your aws credentials. Make a file called `config.aws.json` with the following format:

```javascript
{
	"accessKeyId": "xxx",
	"secretAccessKey": "xxx",
	"region": "us-east-1"
}
```

After the configiration files are generated, please establish the correct settings for them.

# Parameters

 - **--autoscaling-group**
	 - **alias:** `-a`
     - *[Required]*
	 - Name of the AS group to deploy to
 - **--master-instance**
	 - **alias:** `-i`
	 - Instance id of the master to image and copy
 - **--ami-id**
	 - **alias:** `--ami`
	 - Overrides making a new ami by using the provided ami-id instead
 - **--register-targets** 
	 - **alias:** `-r`
	 - *[Boolean]*
	 - Force spawned instances to register to the target group of the AS group. ALB only.
 - **--desired**
	 - **alias:** `-d`
	 - Manually sets the desired capacity during upscaling. The end total, after downscaling, is: `floor(this / 2)`.
 - **--test** 
	 - **alias:** `-t`
	 - *[Boolean]*
	 - Attempts to describe the mi, as, and ami(if provided)
 - **--install**
	 - Commands the install cycle, overrides all other configs and options

# Testing
To test the current configuration, run the following command:

`node delpoy.js -a "as name" -i "instance-id" --ami "ami-id" --test`

The program will *attempt* to describe the parameters passed to it.
`--ami` is optional.

  

# Runtime - What goes on

### 0. Parameters and Configurations are assembled

### 1. A new ami is created

The image made is of the master instance, specified with the parameter `-i` The program will wait until the new image is ready before moving on.

### 2. Count instances in AS Group

The count is of the AS Group specified by the `-a` parameter. Double this number or set it to one before telling the as group to scale up to this desired count. This count is called `goal`.

### 3. Describe the AS Group

Use this information to track old instances and gather the TargetGroupArn if it is attached to an ALB

### 4. Make a new launch configuration

Uses the settings specified in `config.launch.json`, the generated AMI as the image, and the `dateString` gets appended to the end of `config.LaunchConfigurationName`

### 5. Update the AS Group

Assign the new launch config to the AS group, setting the `DesiredCapacity` and `MaxSize` to `goal`. After the update is sent through, the program waits until the `DesiredCapacity` is reached by periodically describing the AS group.

### 6. Await all new instances Running State

As they come online, targets are registered to their ALB if the boolean is enabled

### 7. Downscale

Sets the `DesiredCapacity` and `MaxSize` to `floor(goal / 2) || 1` of the AS group. The program does not wait for downscaling to be complete.