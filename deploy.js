const Event = require('events')
const fs = require("fs")
const chalk = require('chalk')
const aws = require("aws-sdk")
//aws.config.logger = console

const yargs = require("yargs")
    .option('master-instance', {
        alias: 'i',
        desc: "Instance id of the master to image and copy"
    })
    .option('autoscaling-group', {
        alias: 'a',
        desc: "Name of the AS group to deploy to"
    })
    .option('ami-id', {
        alias: 'ami',
        desc: "Overrides making a new ami by using the provided ami-id instead"
    })
    .option('register-targets', {
        alias: 'r',
        desc: "Force spawned instances to register to the target group of the AS group. ALB only.",
        type: 'boolean'
    })
    .option('desired', {
        alias: 'd',
        desc: "Manually sets the desired capacity during upscaling. The end total, after downscaling, is: `floor(this / 2)`"
    })
    .option('test', {
        alias: 't',
        desc: "Attempts to describe the mi, as, and ami(if provided).",
        type: 'boolean'
    })
    .option('install', {
        desc: "Commands the install cycle, overrides all other configs and options",
        type: 'boolean'
    })
    // IMPR: Make the demand options during init() that way they can be dynamic
    /*.demandOption(
        ['autoscaling-group'], 
        "The autoscaling group name must be provided."
    )*/
    .help()
    .argv

let ec2 = new aws.EC2()
let as = new aws.AutoScaling()
let elbv2 = new aws.ELBv2()

// IMPR: Redo this section, cleanup old stuff -fully-
const DC_FILE_PATH = "./config.deploy.json"
let BDC_FILE_PATH = "./config.blockdevicemappings.json"
let LC_FILE_PATH = "./config.launch.json"
let LAUNCH_CONFIG;
let BLOCK_DEVICE_MAPPINGS;
const DEBUG_INSTANCE = "i-044cf149018025b3c"
let AS_GROUP_NAME = "NPL PreProduction"
let AMI_PREFIX = "NPL PreProduction "
let DELAY_MULT = 1000

// --- DEBUG OVERRIDES
var util = require('util');
var logFile = fs.createWriteStream('.log', { flags: 'w' });
var logStdout = process.stdout;

// IMPR: Log titles to console, details to log
console.log = function () {
  logFile.write(util.format.apply(null, arguments) + '\n');
  //logStdout.write(util.format.apply(null, arguments) + '\n');
}
console.error = console.log
/*console.error = () => {
    console.log(chalk.red(util.format.apply(null, arguments)))
}*/
// --- END DEBUG OVERRIDES ---

let Time = new Date()
let dateString = (Time.getMonth() + "" + Time.getDate() + Time.getFullYear() + " " + Time.getHours() + "" + Time.getMinutes())

// Class for tracking spawned instances
class Instance extends Event {
    constructor(Iid, TargetGroupArn) {
        super()
        this.RegisterTargets = true
        this.isHealthy = false
        this.Iid = Iid
        this.TGArn = TargetGroupArn
        this.on('running', () => { 
            this.isHealthy = true
            if (this.RegisterTargets) { // For ALBs only
                console.log("[Registering ["+this.Iid+"] as Target]")
                let params = {
                    TargetGroupArn: this.TGArn,
                    Targets: [{Id: this.Iid}]
                }
                elbv2.registerTargets(params, (err, data) => {
                    if (err)
                        console.log(err, err.stack)
                    console.log(JSON.stringify(data,null,4))

                    let DesiredCapacity = 2
                    Instance.prototype.SpawnedInstances++
                    if (Instance.prototype.SpawnedInstances >= DesiredCapacity) {
                        this.emit('capacityReached')
                    }
                })
            }
        })
        this.on('inService', () => {
            console.log("["+this.Iid+"]'s is inService]")
            this.isHealthy = true
        })
    }

    isRunning(awsInstanceState) {
        return (awsInstanceState.Code == 16)
    }
    
    isInService(awsTargetHealth) {
        return (awsTargetHealth.State == "InService"
                || awsTargetHealth.Reason == "Target.ResponseCodeMismatch") // DEBUG
    }
    
    async awaitRunningState(RegisterTargets) {
        this.RegisterTargets = RegisterTargets
        let check = () => {
            this.describeStatus(data => {
                let i = data.InstanceStatuses[0]
                if (i && this.isRunning(i.InstanceState)) {
                    this.emit('running')
                } else {
                    this.awaitRunningState()
                }
            })
        }
        setTimeout.call(this, check, 5 * DELAY_MULT)
    }

    async awaitInService() {
        let check = () => {
            this.describeHealth(data => {
                if (this.isInService(data)) {
                    this.emit('inService')
                } else {
                    this.awaitInService()
                }
            })
        }
        setTimeout.call(this, check, 5 * DELAY_MULT)
    }

    describeStatus(cb) {
        let params = {
            InstanceIds: [this.Iid]
        }
        ec2.describeInstanceStatus(params, (err, data) => {
            if (err)
                console.log(err, err.stack)
            console.log("[Describing ["+this.Iid+"]'s Status]")
            console.log(JSON.stringify(data, null, 4))
            if (cb)
                cb(data)
        })
    }

    // Only supports ALBs
    describeHealth(cb) {
        let params = {
            TargetGroupArn: this.TGArn
        }
        elbv2.describeTargetHealth(params, (err, data) => {
            if (err)
                console.log(err, err.stack)
            console.log("[Describing ["+this.Iid+"]'s Health]")
            data.TargetHealthDescriptions.forEach(desc => {
                if (desc.Target.Id == this.Iid) {
                    console.log(JSON.stringify(desc, null, 4))
                    if (cb)
                        cb(desc.TargetHealth)
                }
            })
        })
    }
}
Instance.prototype.SpawnedInstances = 0;

// Class for tracking generated AMIs
class AMI extends Event {
    constructor(id, onReady) {
        super()
        this.id = id
        this.on('ready', onReady)
    }

    describeStatus(cb) {
        console.log("[Describing ["+this.id+"]'s Status]")
        let params = {
            ImageIds: [this.id]
        }
        ec2.describeImages(params, (err, data) => {
            if (err)
                console.log(err, err.stack)
            console.log(data.Images[0].State)
            if (cb)
                cb(data.Images[0])
        })
    }

    isRunning(desc) {
        return desc.State == "available"
    }

    async awaitRunningState() {
        let check = () => {
            this.describeStatus(data => {
                if (this.isRunning(data)) {
                    this.emit('ready')
                } else {
                    this.awaitRunningState()
                }
            })
        }
        setTimeout.call(this, check, 5 * DELAY_MULT)
    }
}

// Class for tracking instances in an AS group
class AutoScalingGroup extends Event {
    constructor(instances, desiredCapacity, onAllInServce, onCapacityReached) {
        super()
        this.instances = instances || []
        this.desiredCapacity = desiredCapacity || 1
        this.on('allInService', onAllInServce || (() => {}))
        this.on('capactiyReached', onCapacityReached || (() => {}))
    }

    async awaitInstancesInService() {
        let check = () => {
            let allAreHealthy = (this.instances
                                .filter(e => {return e.isHealthy == false})
                                .length == 0)
            if (allAreHealthy) {
                this.emit('allInService')
            } else {
                this.awaitInstancesInService()
            }
        }
        setTimeout.call(this, check, 20 * DELAY_MULT)
    }

    async awaitDesiredCapacity() {
        let check = () => {
            let params = {
                AutoScalingGroupNames: [AS_GROUP_NAME]
            }
            as.describeAutoScalingGroups(params, (err, data) => {
                console.log("[Current Instance Count: " + data.AutoScalingGroups[0].Instances.length + "/"+this.desiredCapacity+"]")
                let reached = (data.AutoScalingGroups[0].Instances.length >= this.desiredCapacity)
                if (reached) {
                    this.emit('capactiyReached')
                } else {
                    this.awaitDesiredCapacity()
                }
            })
        }
        setTimeout.call(this, check, 5 * DELAY_MULT)
    }
}

// A helper to get the new elements between two arrays by comparing the differentiator
//  - o: array - smaller array
//  - n: array - more verbose array
//  - differentiator: string - parameter to compare
//  - debug: boolean - logs will be printed if true
let getDifference = (o, n, differentiator, debug) => {
    let result = []
    let compare = []
    o.forEach(e => {
        compare[e[differentiator]] = e
    })
    n.forEach(e => {
        if (!(compare[e[differentiator]]))
            result.push(e)
    })
    if (debug) {
        console.log("[Compare]: ", JSON.stringify(compare, null, 4))
        console.log("[Result]: ", JSON.stringify(result, null, 4))
    } 
    return result
}

// Updates AS Group, checkout the runtime section of the readme for a breakdown
//  -   newAMI: string - AMI Id to assign to the LaunchConfiguration
//  -   DesiredCapacity: int - Number of instances to scale up to
//  -   RegisterTargets: boolean - Force Spawned instance to registers the the AS's TargetGroup
let updateAutoScaler = (newAMI, DesiredCapacity, RegisterTargets) => {
    console.log("[Describing AS Group]")
    let params = {
        AutoScalingGroupNames: [AS_GROUP_NAME]
    }
    as.describeAutoScalingGroups(params, (err,data) => {
        if (err)
            console.log(err)
        console.log(JSON.stringify(data, null, 4))

        if (!data || !data.AutoScalingGroups) {
            console.error(chalk.red("Failed to retrieve AS group, exiting..."))
            return -1;
        }

        let TargetArn = data.AutoScalingGroups[0].TargetGroupARNs ? data.AutoScalingGroups[0].TargetGroupARNs[0] : ""
        let instances = data.AutoScalingGroups[0].Instances
        let goal = DesiredCapacity
        
        console.log("[Making new Launch Config]")
        let params = LAUNCH_CONFIG
        params.ImageId = newAMI
        params.LaunchConfigurationName += dateString
        const LC_NAME = params.LaunchConfigurationName

        as.createLaunchConfiguration(params, (err, data) => {
            if (err) 
                console.log(err, err.stack); 
            console.log(JSON.stringify(data,null,4));
            
            console.log("[Updating Autoscaling Group["+AS_GROUP_NAME+"]]")
            let params = {
                AutoScalingGroupName: AS_GROUP_NAME,
                DesiredCapacity: goal,
                MaxSize: goal, 
                LaunchConfigurationName: LC_NAME
            }
            as.updateAutoScalingGroup(params, (err,data) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log(JSON.stringify(data, null, 4))

                    // The update comes through fast, but not instant. Wait for it.
                    new AutoScalingGroup(null, goal, ()=>{}, () => {
                        console.log("[Describing Autoscaling Group["+AS_GROUP_NAME+"]]")
                        let params = {
                            AutoScalingGroupNames: [AS_GROUP_NAME]
                        }
                        as.describeAutoScalingGroups(params, (err, data) => {
                            if (err)
                                console.log(err)
                            console.log(JSON.stringify(data, null, 4))

                            // Track the new instances 
                            let TrackedInstances = getDifference(instances, data.AutoScalingGroups[0].Instances, 'InstanceId')
                                .map(
                                    e => {
                                        let i = new Instance(e.InstanceId, TargetArn)
                                        i.awaitRunningState(RegisterTargets) // IMPR: convert to i.awaitInService()
                                        return i
                                    }
                                )

                            // IMPR: Migrate this to the above, already made object
                            new AutoScalingGroup(TrackedInstances, goal, () => {
                                let desired = 1
                                console.log("[Setting Desired Capacity to: " + desired + "]")
                                let params = {
                                    AutoScalingGroupName: AS_GROUP_NAME,
                                    DesiredCapacity: desired,
                                    MaxSize: desired, 
                                }
                                as.updateAutoScalingGroup(params, (err, data) => {
                                    if (err)
                                        console.log(err, err.stack)
                                    console.log(JSON.stringify(data, null, 4))

                                    // await downscaling? done at this point
                                })
                            }).awaitInstancesInService()
                        })
                    }).awaitDesiredCapacity()
                }
            })
        });
    })
}

// Generates a new AMI of the InstanceId passed
//  -   InstanceId: string - Instance Id of the master instance to image 
//  -   cb: (ImageId) => {} - Callback function where the AMI's Id is passed
let makeNewAMI = (InstanceId, cb) => {
    console.log("[Making AMI]")
    let BlockDeviceMappings = BLOCK_DEVICE_MAPPINGS
    let params = {
        BlockDeviceMappings: BlockDeviceMappings,
        Description: AMI_PREFIX  + dateString,
        InstanceId: InstanceId,
        Name: AMI_PREFIX  + dateString,
        NoReboot: true
    }
    ec2.createImage(params, (err, data) => {
        if (err) {
            console.log(err, err.stack)
        } else {
            console.log(JSON.stringify(data, null, 4))

            if (cb)
                cb(data.ImageId)
        }
    })
}

// Counts the number of instances in the AS Group
//  -   cb : (Instance_Count) => {} - Callback function
let countInstancesInAS = (cb) => {
    console.log("[Counting Instances in [" + AS_GROUP_NAME + "] Group]")
    let params = {
        AutoScalingGroupNames: [AS_GROUP_NAME]
    }
    as.describeAutoScalingGroups(params, (err, data) => {
        if (err)
            console.log(err, err.stack)
        console.log(JSON.stringify(data.AutoScalingGroups[0].Instances.length, null, 4))
        cb(data.AutoScalingGroups[0].Instances.length)
    })
}

// Attempts to describe each parameter to test the current configuration
//  -   instanceToImage: string - InstanceID of the master instance
//  -   autoScalingGroup: string - ASG name 
//  -   amiID: string - Ami Id to check 
//  -   cb: () => {} - Callback function
let testConfiguration = (instanceToImage, autoScalingGroup, amiId, cb) => {
    let errorCount = 0
    let finalOutput = (errorCount, totalCount) => {
        if (errorCount > 0) {
            console.warn(chalk.yellow("Testing yielded " + chalk.bold(errorCount + "/" + totalCount) + " failures."))
        } else {
            console.warn("All " + chalk.bold(totalCount) + " tests returned " + chalk.green("successfully!"))
        }
        if (cb)
            cb(errorCount)
    }

    let params = {
        InstanceIds: [instanceToImage]
    }
    ec2.describeInstanceStatus(params, (err, data) => {
        if (err) {
            console.log(err, err.stack)
            console.warn("Failed to describe " + chalk.bold("instance") + "!")
            errorCount++
        } 
        let params = {
            AutoScalingGroupNames: [autoScalingGroup]
        }
        as.describeAutoScalingGroups(params, (err, data) => {
            if (err) {
                console.log(err, err.stack)
                console.warn("Failed to describe " + chalk.bold("AS group") + "!")
                errorCount++                    
            }
            
            if (amiId) {
                let params = {
                    ImageIds: [amiId]
                }
                ec2.describeImages(params, (err, data) => {
                    if (err) {
                        console.log(err, err.stack)
                        console.warn("Failed to describe " + chalk.bold("AMI") + "!")
                        errorCount++
                    }
                    finalOutput(errorCount, 3)
                })
            } else {
                finalOutput(errorCount, 2)
            }
        })
    })
}

// The main deployment function that runs the update methods
//  -   instanceToImage: string - InstanceID of the master instance
//  -   registerTargets: boolean - if flagged, instances are forced to register to the AS's target group
//  -   desired: int - Manual override for the upscaling count of instances. Downscaled to floor(this/2) at the end.
//  -   amiOverride: string - AMI Id to use instead of generating and image from the master instance.
// IMPR: Log titles to console, and dont log ansi encodings 
let deploy = (instanceToImage, registerTargets, desired, amiOverride) => {
    console.log(dateString)
    if (!amiOverride) {
        makeNewAMI(instanceToImage, ami => {
            new AMI(ami, () => {
                countInstancesInAS(count => {
                    let goal = desired || (count * 2 || 1)
                    updateAutoScaler(ami, goal, registerTargets)
                })
            }).awaitRunningState()
        })
    } else {
        countInstancesInAS(count => {
            let goal = desired || (count * 2 || 1)
            updateAutoScaler(amiOverride, goal, registerTargets)
        })
    }
};

// Generates the configuration file for the LaunchConfiguration
let generateDefaultLaunchConfigFile = () => {
    let defaultConfig = {
        "InstanceType": "t2.micro",
        "KeyName": "MMKeypair",
        "LaunchConfigurationName": "NPL PreProd - ", // Date string gets added later
        "SecurityGroups": [
            "sg-35828252"
        ]
    }
    return fs.writeFileSync(LC_FILE_PATH, JSON.stringify(defaultConfig, null, 4))
}

// Generates the configuration file for new AMI's BlockDeviceMappings
let generateDefaultBlockDeviceFile = () => {
    let defaultConfig = [{
        "DeviceName": "/dev/sda1",
        "Ebs": {
            "DeleteOnTermination": true,
            "VolumeSize": 30,
            "VolumeType": "gp2"
        }
    },
    {
        "DeviceName": "xvdb",
        "Ebs": {
            "DeleteOnTermination": true,
            "VolumeSize": 200,
            "VolumeType": "gp2"
        }
    }]
    return fs.writeFileSync(BDC_FILE_PATH, JSON.stringify(defaultConfig, null, 4))
}

// Generates the configuration file for deploy.js
let generateDefaultDeployConfigFile = () => {
    let defaultConfig = {
        "BDC_FILE_PATH": BDC_FILE_PATH,
        "LC_FILE_PATH": LC_FILE_PATH,
        "AMI_PREFIX": "NPL PreProduction ",
        "DELAY_MULT": 1000
    }
    return fs.writeFileSync(DC_FILE_PATH, JSON.stringify(defaultConfig, null, 4))
}

//  Generates the nessecary config files
// [Depreciated]
let install = () => {
    console.warn("Writing Config Files Locally")
    console.log("[Config files rewritten]")
    generateDefaultDeployConfigFile()
    generateDefaultBlockDeviceFile()
    generateDefaultLaunchConfigFile()
}

// The first thing that is called
let init = () => {
    // Check if in install mode
    if (yargs.install) {
        install()
        return
    // Check if required parameters are provided
    } else if (!yargs.a) {
        console.warn(chalk.yellow("Auto Scaling Group parameter was not provided!"))
        return 
    } else {

        // Gather parameters
        let instanceToImage = (yargs.i  == "debug") ? DEBUG_INSTANCE : yargs.i
        AS_GROUP_NAME = (yargs.a == "debug") ? AS_GROUP_NAME : yargs.a
        let desired = yargs.desired || 0 
        let register = yargs.r || false

        // Gather Configs
        if (!fs.existsSync(DC_FILE_PATH)) {
            console.warn(chalk.yellow("Could not find ("
                                        + chalk.cyan.bold(DC_FILE_PATH) 
                                        + ")!" 
                                        + " Generating it with default settings."))
            generateDefaultDeployConfigFile()
        }
        let settings = require(DC_FILE_PATH)
        console.warn(JSON.stringify(settings, null, 4))
        BDC_FILE_PATH = settings.BDC_FILE_PATH || BDC_FILE_PATH
        LC_FILE_PATH = settings.LC_FILE_PATH || LC_FILE_PATH
        AMI_PREFIX = settings.AMI_PREFIX || AMI_PREFIX
        DELAY_MULT = settings.DELAY_MULT || DELAY_MULT

        if (!fs.existsSync(BDC_FILE_PATH)) {
            console.warn(chalk.yellow("Could not find ("
                                        + chalk.cyan.bold(BDC_FILE_PATH) 
                                        + ")!" 
                                        + " Generating it with default settings."))
            generateDefaultBlockDeviceFile()
        }
        BLOCK_DEVICE_MAPPINGS = require(BDC_FILE_PATH)

        if (!fs.existsSync(LC_FILE_PATH)) {
            console.warn(chalk.yellow("Could not find ("
                                        + chalk.cyan.bold(LC_FILE_PATH) 
                                        + ")!"
                                        + " Generating it with default settings."))
            generateDefaultLaunchConfigFile()
        }
        LAUNCH_CONFIG = require(LC_FILE_PATH)

        aws.config.loadFromPath('./config.aws.json')

        // Run Deployment
        if (yargs.test) {
            testConfiguration(instanceToImage, yargs.as, yargs.ami)
        } else {
            deploy(instanceToImage, register, desired, yargs.ami)
        }
    }
}
init()

// node deploy.js -i debug -a debug --test